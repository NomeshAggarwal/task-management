from application.models import db, tasks


class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(70), nullable=False)
    email = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    role = db.Column(db.String(70), nullable=False, default='Developer')
    # tasks = db.relationship('Task', backref=db.backref('employee'))
    # manager = db.relationship('EmpManager', backref=db.backref('manager'))

    def __init__(self, name, email, password, role):
        self.name = name
        self.email = email
        self.password = password
        self.role = role
