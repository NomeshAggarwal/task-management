from application.models import db


class EmpManager(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)
    manager_id = db.Column(db.Integer, nullable=False)
    employee = db.relationship('Employee', backref=db.backref('EmpManager'))
    def __init__(self, employee_id, manager_id):
        self.employee_id = employee_id
        self.manager_id = manager_id
