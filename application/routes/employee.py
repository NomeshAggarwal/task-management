from flask import jsonify, request
from application.routes import app, token_required
from application.models import manager
from application.utilities import serialize


@app.route('/manager/employees')
@token_required
def get_employees_for_manager():
    try:
        mydata = manager.EmpManager.query.filter_by(manager_id=request.data.id).all()
        return '%s' % serialize(mydata)
    except Exception as e:
        print(e)
        return jsonify({'message': "You're not manager of anyone"})
